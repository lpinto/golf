<?php

namespace App\Tests\Utils;

use App\Utils\UtilJson;
use PHPUnit\Framework\TestCase;

class UtilJsonTest extends TestCase
{

    public function testNbJoueursTotal()
    {
        $uj = new UtilJson();

        $expectedJson = 118;
        $this->assertEquals($expectedJson, $uj->toJsonCount($uj->parsed_json));
    }

    public function testRep()
    {
        $debut = 0; //0, 42, 50
        $uj = new UtilJson();

        $expectedJson = 42; //42, 8, 68
        $this->assertEquals($expectedJson, $uj->CountRepJoueur($debut, $uj->parsed_json[$debut]->rep));
    }

    public function testTabRep(){
        $uj = new UtilJson();

        $expectedJson = [42, 8, 68];
        $this->assertEquals($expectedJson,$uj->TabRep());
    }

    public function testMult3(){
        $uj = new UtilJson();

//        $expectedJson = [14, 2, 1, 21, 2];
        $expectedJson = [14, 2, 1, 22, 1];
        $this->assertEquals($expectedJson,$uj->TabMult3());
    }
}
