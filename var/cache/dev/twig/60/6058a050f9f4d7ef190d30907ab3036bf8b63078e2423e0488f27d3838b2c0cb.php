<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ajout/informations.html.twig */
class __TwigTemplate_caeb98b3978ead0bd9c39e106593e2cbc5174f8102cd8f544504b0fbdea0a47d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ajout/informations.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ajout/informations.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "ajout/informations.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Cadence de jeu";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"container\">
        <h1 class=\"major\">Remplir les informations </h1>

        <form action=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("form_trou", ["golf_id" => (isset($context["golf_id"]) || array_key_exists("golf_id", $context) ? $context["golf_id"] : (function () { throw new RuntimeError('Variable "golf_id" does not exist.', 9, $this->source); })())]), "html", null, true);
        echo "\" method=\"post\" class=\"formTrou\">
            <div class=\"row\">
                <div class=\"col-sm\">
                    <label for=\"trou1\">Trou 1</label>
                    <input type=\"text\" name=\"trou1\" id=\"trou1\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou1\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou1\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou1\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou2\">Trou 2</label>
                    <input type=\"text\" name=\"trou2\" id=\"trou2\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou2\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou2\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou2\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou3\">Trou 3</label>
                    <input type=\"text\" name=\"trou3\" id=\"trou3\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou3\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou3\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou3\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou4\">Trou 4</label>
                    <input type=\"text\" name=\"trou4\" id=\"trou4\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou4\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou4\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou4\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou5\">Trou 5</label>
                    <input type=\"text\" name=\"trou5\" id=\"trou5\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou5\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou5\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou5\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou6\">Trou 6</label>
                    <input type=\"text\" name=\"trou6\" id=\"trou6\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou6\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou6\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou6\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                </div>
                <div class=\"col-sm\">
                    <label for=\"trou7\">Trou 7</label>
                    <input type=\"text\" name=\"trou7\" id=\"trou7\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou7\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou7\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou7\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou8\">Trou 8</label>
                    <input type=\"text\" name=\"trou8\" id=\"trou8\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou8\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou8\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou8\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou9\">Trou 9</label>
                    <input type=\"text\" name=\"trou9\" id=\"trou9\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou9\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou9\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou9\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou10\">Trou 10</label>
                    <input type=\"text\" name=\"trou10\" id=\"trou10\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou10\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou10\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou10\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou11\">Trou 11</label>
                    <input type=\"text\" name=\"trou11\" id=\"trou11\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou11\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou11\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou11\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou12\">Trou 12</label>
                    <input type=\"text\" name=\"trou12\" id=\"trou12\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou12\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou12\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou12\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                </div>
                <div class=\"col-sm\">
                    <label for=\"trou13\">Trou 13</label>
                    <input type=\"text\" name=\"trou13\" id=\"trou13\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou13\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou13\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou13\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou14\">Trou 14</label>
                    <input type=\"text\" name=\"trou14\" id=\"trou14\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou14\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou14\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou14\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou15\">Trou 15</label>
                    <input type=\"text\" name=\"trou15\" id=\"trou15\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou15\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou15\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou15\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou16\">Trou 16</label>
                    <input type=\"text\" name=\"trou16\" id=\"trou16\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou16\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou16\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou16\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou17\">Trou 17</label>
                    <input type=\"text\" name=\"trou17\" id=\"trou17\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou17\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou17\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou17\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou18\">Trou 18</label>
                    <input type=\"text\" name=\"trou18\" id=\"trou18\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou18\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou18\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou18\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                </div>
            </div>
            <br>
            <button type=\"submit\" class=\"btn btn-success\">Envoyer</button>
            <br>
            <br>
            <br>


        </form>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ajout/informations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Cadence de jeu{% endblock %}

{% block body %}
    <div class=\"container\">
        <h1 class=\"major\">Remplir les informations </h1>

        <form action=\"{{ path('form_trou', {'golf_id': golf_id}) }}\" method=\"post\" class=\"formTrou\">
            <div class=\"row\">
                <div class=\"col-sm\">
                    <label for=\"trou1\">Trou 1</label>
                    <input type=\"text\" name=\"trou1\" id=\"trou1\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou1\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou1\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou1\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou2\">Trou 2</label>
                    <input type=\"text\" name=\"trou2\" id=\"trou2\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou2\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou2\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou2\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou3\">Trou 3</label>
                    <input type=\"text\" name=\"trou3\" id=\"trou3\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou3\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou3\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou3\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou4\">Trou 4</label>
                    <input type=\"text\" name=\"trou4\" id=\"trou4\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou4\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou4\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou4\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou5\">Trou 5</label>
                    <input type=\"text\" name=\"trou5\" id=\"trou5\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou5\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou5\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou5\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou6\">Trou 6</label>
                    <input type=\"text\" name=\"trou6\" id=\"trou6\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou6\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou6\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou6\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                </div>
                <div class=\"col-sm\">
                    <label for=\"trou7\">Trou 7</label>
                    <input type=\"text\" name=\"trou7\" id=\"trou7\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou7\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou7\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou7\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou8\">Trou 8</label>
                    <input type=\"text\" name=\"trou8\" id=\"trou8\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou8\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou8\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou8\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou9\">Trou 9</label>
                    <input type=\"text\" name=\"trou9\" id=\"trou9\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou9\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou9\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou9\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou10\">Trou 10</label>
                    <input type=\"text\" name=\"trou10\" id=\"trou10\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou10\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou10\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou10\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou11\">Trou 11</label>
                    <input type=\"text\" name=\"trou11\" id=\"trou11\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou11\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou11\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou11\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou12\">Trou 12</label>
                    <input type=\"text\" name=\"trou12\" id=\"trou12\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou12\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou12\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou12\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                </div>
                <div class=\"col-sm\">
                    <label for=\"trou13\">Trou 13</label>
                    <input type=\"text\" name=\"trou13\" id=\"trou13\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou13\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou13\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou13\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou14\">Trou 14</label>
                    <input type=\"text\" name=\"trou14\" id=\"trou14\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou14\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou14\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou14\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou15\">Trou 15</label>
                    <input type=\"text\" name=\"trou15\" id=\"trou15\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou15\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou15\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou15\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou16\">Trou 16</label>
                    <input type=\"text\" name=\"trou16\" id=\"trou16\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou16\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou16\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou16\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou17\">Trou 17</label>
                    <input type=\"text\" name=\"trou17\" id=\"trou17\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou17\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou17\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou17\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                    <br>
                    <br>
                    <label for=\"trou18\">Trou 18</label>
                    <input type=\"text\" name=\"trou18\" id=\"trou18\" class=\"form-control\" placeholder=\"Temps de marche\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou18\" id=\"par3\" value=\"1\" checked>
                        <label class=\"form-check-label\" for=\"par3\"> Par 3 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou18\" id=\"par4\" value=\"2\">
                        <label class=\"form-check-label\" for=\"par4\"> Par 4 </label>
                    </div>
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"parTrou18\" id=\"par5\" value=\"3\">
                        <label class=\"form-check-label\" for=\"par5\"> Par 5 </label>
                    </div>
                </div>
            </div>
            <br>
            <button type=\"submit\" class=\"btn btn-success\">Envoyer</button>
            <br>
            <br>
            <br>


        </form>
    </div>
{% endblock %}", "ajout/informations.html.twig", "D:\\Programmes\\laragon\\www\\Symfony\\Mission1\\golf\\templates\\ajout\\informations.html.twig");
    }
}
