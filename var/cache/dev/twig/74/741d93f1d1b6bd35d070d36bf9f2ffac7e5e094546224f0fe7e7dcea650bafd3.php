<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* to_pdf/index.html.twig */
class __TwigTemplate_ddefa8c2f38864c70839a01837519b90709ca5ca39492b63429eaf768ffe1278 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "to_pdf/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "to_pdf/index.html.twig"));

        // line 1
        echo "<style>
    @page {
        margin: 50px 25px 25px 25px;
        font-family: Arial, sans-serif;
    }

    body {
        margin: 0 0 1em 0;
    }

    table, th, tr, td {
        border: 1px solid #000000;
        border-collapse: collapse;
        font-size: 9px;
        line-height: 10px;
    }

    table {
        width: 100%;
    }

    thead {
        background-color: yellow;
    }

    .index {
        text-align: center;
        width: 17px;
    }

    .indexC {
        text-align: center;
    }

    .dep {
        text-align: center;
        font-weight: bold;
    }

    .trou {
        text-align: center;
        width: 40px;
    }

    .trouC {
        vertical-align: top;
        text-align: right;
    }

    .joueur {
        text-align: center;
    }
</style>


";
        // line 56
        $context["starter"] = (isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 56, $this->source); })());
        // line 57
        echo "

<br>
<table>
    <thead>
    <tr>
        <th class=\"index\">#</th>
        <th class=\"trou\">Dép.</th>
        <th class=\"joueur\"> Joueurs</th>
        <th class=\"trou\"> 1</th>
        <th class=\"trou\"> 2</th>
        <th class=\"trou\"> 3</th>
        <th class=\"trou\"> 4</th>
        <th class=\"trou\"> 5</th>
        <th class=\"trou\"> 6</th>
        <th class=\"trou\"> 7</th>
        <th class=\"trou\"> 8</th>
        <th class=\"trou\"> 9</th>
        <th class=\"trou\"> 10</th>
        <th class=\"trou\"> 11</th>
        <th class=\"trou\"> 12</th>
        <th class=\"trou\"> 13</th>
        <th class=\"trou\"> 14</th>
        <th class=\"trou\"> 15</th>
        <th class=\"trou\"> 16</th>
        <th class=\"trou\"> 17</th>
        <th class=\"trou\"> 18</th>
        <th class=\"index\">#</th>
    </tr>
    </thead>
    <tbody>

    ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabJoueurs"]) || array_key_exists("tabJoueurs", $context) ? $context["tabJoueurs"] : (function () { throw new RuntimeError('Variable "tabJoueurs" does not exist.', 89, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 90
            echo "        ";
            $context["tempsTrou"] = (isset($context["starter"]) || array_key_exists("starter", $context) ? $context["starter"] : (function () { throw new RuntimeError('Variable "starter" does not exist.', 90, $this->source); })());
            // line 91
            echo "        <tr>

            <td class=\"indexC\"> ";
            // line 93
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 93), "html", null, true);
            echo " </td>
            <td class=\"dep\">
                ";
            // line 95
            $context["heures"] = (int) floor(((isset($context["starter"]) || array_key_exists("starter", $context) ? $context["starter"] : (function () { throw new RuntimeError('Variable "starter" does not exist.', 95, $this->source); })()) / 60));
            // line 96
            echo "                ";
            $context["minutes"] = ((((isset($context["starter"]) || array_key_exists("starter", $context) ? $context["starter"] : (function () { throw new RuntimeError('Variable "starter" does not exist.', 96, $this->source); })()) / 60) - (int) floor(((isset($context["starter"]) || array_key_exists("starter", $context) ? $context["starter"] : (function () { throw new RuntimeError('Variable "starter" does not exist.', 96, $this->source); })()) / 60))) * 60);
            // line 97
            echo "                ";
            echo twig_escape_filter($this->env, (isset($context["heures"]) || array_key_exists("heures", $context) ? $context["heures"] : (function () { throw new RuntimeError('Variable "heures" does not exist.', 97, $this->source); })()), "html", null, true);
            echo ":
                ";
            // line 98
            if ((twig_length_filter($this->env, (isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 98, $this->source); })())) == 1)) {
                // line 99
                echo "                    0";
                echo twig_escape_filter($this->env, (isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 99, $this->source); })()), "html", null, true);
                echo "
                ";
            } else {
                // line 101
                echo "                    ";
                echo twig_escape_filter($this->env, (isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 101, $this->source); })()), "html", null, true);
                echo "
                ";
            }
            // line 103
            echo "
            </td>
            <td>
                ";
            // line 106
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["info"]);
            foreach ($context['_seq'] as $context["_key"] => $context["joueur"]) {
                // line 107
                echo "                    ";
                echo twig_escape_filter($this->env, $context["joueur"], "html", null, true);
                echo " <br>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['joueur'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 109
            echo "            </td>

            ";
            // line 111
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["golfs"]) || array_key_exists("golfs", $context) ? $context["golfs"] : (function () { throw new RuntimeError('Variable "golfs" does not exist.', 111, $this->source); })()), "Trous", [], "any", false, false, false, 111));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 112
                echo "                <td class=\"trouC\">
                    ";
                // line 113
                $context["tempsTrou"] = (((isset($context["tempsTrou"]) || array_key_exists("tempsTrou", $context) ? $context["tempsTrou"] : (function () { throw new RuntimeError('Variable "tempsTrou" does not exist.', 113, $this->source); })()) + twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["i"], "Pars", [], "any", false, false, false, 113), "Temps", [], "any", false, false, false, 113)) + twig_get_attribute($this->env, $this->source, $context["i"], "TempsMarche", [], "any", false, false, false, 113));
                // line 114
                echo "                    ";
                $context["heures"] = (int) floor(((isset($context["tempsTrou"]) || array_key_exists("tempsTrou", $context) ? $context["tempsTrou"] : (function () { throw new RuntimeError('Variable "tempsTrou" does not exist.', 114, $this->source); })()) / 60));
                // line 115
                echo "                    ";
                $context["minutes"] = ((((isset($context["tempsTrou"]) || array_key_exists("tempsTrou", $context) ? $context["tempsTrou"] : (function () { throw new RuntimeError('Variable "tempsTrou" does not exist.', 115, $this->source); })()) / 60) - (int) floor(((isset($context["tempsTrou"]) || array_key_exists("tempsTrou", $context) ? $context["tempsTrou"] : (function () { throw new RuntimeError('Variable "tempsTrou" does not exist.', 115, $this->source); })()) / 60))) * 60);
                // line 116
                echo "                    ";
                echo twig_escape_filter($this->env, (isset($context["heures"]) || array_key_exists("heures", $context) ? $context["heures"] : (function () { throw new RuntimeError('Variable "heures" does not exist.', 116, $this->source); })()), "html", null, true);
                echo ":
                    ";
                // line 117
                if ((twig_length_filter($this->env, (isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 117, $this->source); })())) == 1)) {
                    // line 118
                    echo "                        0";
                    echo twig_escape_filter($this->env, twig_round((isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 118, $this->source); })()), 0, "floor"), "html", null, true);
                    echo "
                    ";
                } else {
                    // line 120
                    echo "                        ";
                    echo twig_escape_filter($this->env, twig_round((isset($context["minutes"]) || array_key_exists("minutes", $context) ? $context["minutes"] : (function () { throw new RuntimeError('Variable "minutes" does not exist.', 120, $this->source); })()), 0, "floor"), "html", null, true);
                    echo "
                    ";
                }
                // line 122
                echo "                </td>

            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "

            <td class=\"indexC\"> ";
            // line 127
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 127), "html", null, true);
            echo " </td>
        </tr>
        ";
            // line 129
            $context["starter"] = ((isset($context["starter"]) || array_key_exists("starter", $context) ? $context["starter"] : (function () { throw new RuntimeError('Variable "starter" does not exist.', 129, $this->source); })()) + 11);
            // line 130
            echo "
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "    </tbody>
</table>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "to_pdf/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  279 => 132,  264 => 130,  262 => 129,  257 => 127,  253 => 125,  245 => 122,  239 => 120,  233 => 118,  231 => 117,  226 => 116,  223 => 115,  220 => 114,  218 => 113,  215 => 112,  211 => 111,  207 => 109,  198 => 107,  194 => 106,  189 => 103,  183 => 101,  177 => 99,  175 => 98,  170 => 97,  167 => 96,  165 => 95,  160 => 93,  156 => 91,  153 => 90,  136 => 89,  102 => 57,  100 => 56,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
    @page {
        margin: 50px 25px 25px 25px;
        font-family: Arial, sans-serif;
    }

    body {
        margin: 0 0 1em 0;
    }

    table, th, tr, td {
        border: 1px solid #000000;
        border-collapse: collapse;
        font-size: 9px;
        line-height: 10px;
    }

    table {
        width: 100%;
    }

    thead {
        background-color: yellow;
    }

    .index {
        text-align: center;
        width: 17px;
    }

    .indexC {
        text-align: center;
    }

    .dep {
        text-align: center;
        font-weight: bold;
    }

    .trou {
        text-align: center;
        width: 40px;
    }

    .trouC {
        vertical-align: top;
        text-align: right;
    }

    .joueur {
        text-align: center;
    }
</style>


{% set starter = minutes %}


<br>
<table>
    <thead>
    <tr>
        <th class=\"index\">#</th>
        <th class=\"trou\">Dép.</th>
        <th class=\"joueur\"> Joueurs</th>
        <th class=\"trou\"> 1</th>
        <th class=\"trou\"> 2</th>
        <th class=\"trou\"> 3</th>
        <th class=\"trou\"> 4</th>
        <th class=\"trou\"> 5</th>
        <th class=\"trou\"> 6</th>
        <th class=\"trou\"> 7</th>
        <th class=\"trou\"> 8</th>
        <th class=\"trou\"> 9</th>
        <th class=\"trou\"> 10</th>
        <th class=\"trou\"> 11</th>
        <th class=\"trou\"> 12</th>
        <th class=\"trou\"> 13</th>
        <th class=\"trou\"> 14</th>
        <th class=\"trou\"> 15</th>
        <th class=\"trou\"> 16</th>
        <th class=\"trou\"> 17</th>
        <th class=\"trou\"> 18</th>
        <th class=\"index\">#</th>
    </tr>
    </thead>
    <tbody>

    {% for info in tabJoueurs %}
        {% set tempsTrou = starter %}
        <tr>

            <td class=\"indexC\"> {{ loop.index }} </td>
            <td class=\"dep\">
                {% set heures = starter // 60 %}
                {% set minutes = (starter/ 60 - starter // 60) * 60 %}
                {{ heures }}:
                {% if minutes|length == 1 %}
                    0{{ minutes }}
                {% else %}
                    {{ minutes }}
                {% endif %}

            </td>
            <td>
                {% for joueur in info %}
                    {{ joueur }} <br>
                {% endfor %}
            </td>

            {% for i in golfs.Trous %}
                <td class=\"trouC\">
                    {% set tempsTrou = tempsTrou + i.Pars.Temps + i.TempsMarche %}
                    {% set heures = tempsTrou // 60 %}
                    {% set minutes = (tempsTrou/ 60 - tempsTrou // 60) * 60 %}
                    {{ heures }}:
                    {% if minutes|length == 1 %}
                        0{{ minutes |round(0, 'floor') }}
                    {% else %}
                        {{ minutes |round(0, 'floor') }}
                    {% endif %}
                </td>

            {% endfor %}


            <td class=\"indexC\"> {{ loop.index }} </td>
        </tr>
        {% set starter = starter + 11 %}

    {% endfor %}
    </tbody>
</table>", "to_pdf/index.html.twig", "D:\\Programmes\\laragon\\www\\Symfony\\Mission1\\golf\\templates\\to_pdf\\index.html.twig");
    }
}
