Nom du projet

PRAGolf – Compétition Project 

Installation :

- git clone https://gitlab.com/lpinto/golf/
- changer son .env pour se connecter à la BDD
- php bin/console doctrine:database:create
- php bin/console make:migration
- php bin/console doctrine:migrations:migrate
- php bin/console doctrine:fixtures:load

Auteur:

Pinto Laetitia
Claustrat Clément
Belhadi Sofiane
Vaillant Valentin

Travail attendu :

Front-end:

Production d’un fichier Excel imprimable permettant de saisir sur le terrain les temps de jeu effectifs de certaines parties, sur certains trous, sous forme d’un décalage (offset) +/- nombre de minutes. Les arbitres ont les cadences de jeu avec eux et notent les temps des parties en décalage sur le papier, des parties qu’ils suivent pour un temps. Pour une bonne occupation du terrain, les équipes doivent avancées « ensemble ».
Un formulaire « cadence_de_jeu » anonymisé (sous forme de tableau) à fin de statistiques, afin de permettre à des arbitres d’inscrire des informations de temps passé par une équipe, pour certains trous.

Back-end:

Gestion de données relatives aux terrains de golf 

Outils utilisés :

PHPstorm
dia (diagramme uml et diagramme d’utilisateur)
phpspreadsheet.

Acteur :

Admin : 
gère le back-end : les données des golfs (nom, localisation, par, trou, temps de déplacement zone du trou i à zone de départ du trou i+1)

Arbitre:
Déclenche une demande de "cadences de jeu" en fournissant un fichier Excel contenant la liste des inscrits, + Golf concerné et intitulé de la compétition, date. 
Il reçoit, in fine, un document PDF dont la sauvegarde est à la charge de l’arbitre (et non du service).
Remonte des données anonymisées qu’il juge utile afin d’alimenter la base de données de temps réellement passés, via le formulaire « remontée de cadences de jeu » anonymisé.


1er phase du projet :

Un premier document d’analyse comprenant (outre les informations classiques comme un titre, auteurs, date, etc.):

Tableau de répartition des tâches entre collaborateurs
Diagramme de classes des entités (classes Entity)
Diagramme des cas d’utilisation (acteurs et services attendus)

Gestion des données d’entrée d’une compétition :
		
Définir la stratégie adoptée pour extraire/structurer les informations du fichier Excel reçu, vers un format JSON.
Décrire la structure retenue (format JSON).

Gestion des données de golfs :

Preuve de la constitution d’un jeu de données pour un golf, données à déduire du document « cadences de jeu » (golf et ses trous)
Identification des constantes de jeu qui seront utilisées par l’application.

2eme phase du projet :

Rapport technique, incluant des copies écran d’un prototype opérationnel, associées à des explications détaillées sur des parties du travail réalisé. La couverture fonctionnelle sera clairement identifiée.
Présence d’une branche de tests
URL vers un dépôt git distant et public, avec un descriptif README.





