<?php

namespace App\Repository;

use App\Entity\Ajout;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ajout|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ajout|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ajout[]    findAll()
 * @method Ajout[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AjoutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ajout::class);
    }

    // /**
    //  * @return Ajout[] Returns an array of Ajout objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ajout
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
