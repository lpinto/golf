<?php

namespace App\Utils;

class UtilJson
{

    public $parsed_json;

    public function __construct()
    {
        // On initialise nos variables
//        $this->parsed_json = json_decode(file_get_contents("../../public/datas.json")); // Pour les Tests Unitaires
        $this->parsed_json = json_decode(file_get_contents("../public/datas.json"));
    }

// Cette fonction va nous permettre de compter le nombre de joueurs.
    public function toJsonCount(array $tab): int
    {
        if (empty($tab)) {
            return 0;
        } else {
            return count($tab);
        }
    }

    public function CountRepJoueur(int $debut, string $rep): int
    {
        $i = $debut;
        $stop = True;
        while ($stop != False && $i < count($this->parsed_json)) {
            if ($this->parsed_json[$i]->rep != $rep) {
                $stop = False;
            }
            $i++;
        }

        if ($i == count($this->parsed_json)) {
            return $i - $debut;
        }
        return ($i - 1) - $debut;
    }

    public function TabRep()
    {
        $debut = 0;
        $tab = [];
        while ($debut < $this->toJsonCount($this->parsed_json)) {
            $temp = $this->CountRepJoueur($debut, $this->parsed_json[$debut]->rep);
            $debut += $temp;
            $tab[] = $temp;
        }
        return $tab;
    }

    public function TabMult3()
    {
        $uj = new UtilJson();
        $TabRep = $uj->TabRep();
//        $TabRep = [42, 8, 67];
        $tabMult3 =[];

        for ($i = 0; $i < count($TabRep); $i++) {
            if ($TabRep[$i] % 3 == 0) {
                $tabMult3[] = $TabRep[$i] / 3;
            } else {
                $temp = $TabRep[$i] % 3;
                if ($temp == 1) {
                    $tabMult3[] = (int)($TabRep[$i] / 3) - 1;
                    $tabMult3[] = 2;
                } else {
                    $tabMult3[] = (int)($TabRep[$i] / 3);
                    $tabMult3[] = 1;
                }
            }
        }
        return $tabMult3;
    }


}