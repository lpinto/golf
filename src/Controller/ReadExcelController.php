<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * for all controller methods in this class
 *@IsGranted("ROLE_USER")
 */
class ReadExcelController extends AbstractController
{
    /**
     * @Route("/read", name="read_excel")
     */
    public function readFile()
    {
        $inputFileName = '../public/file_uploads/file.xlsx';

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);

        //On récupère toutes les infos des cellules B1 à B1000
        $cellName = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                'B1:B1000',     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                false         // Should the array be indexed by cell row and cell column
            );
        //On récupère le nom de la compétition
        $cellNameC = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                'B10',     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                false         // Should the array be indexed by cell row and cell column
            );

        //On récupère la date de la compétition
        $cellDate = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                'E4',     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                false         // Should the array be indexed by cell row and cell column
            );

        //On récupère toutes les infos des cellules I1 à I1000
        $cellRep = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                'I1:I1000',     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                false         // Should the array be indexed by cell row and cell column
            );

        $tabNom = array();
        $tabRep = array();


        $sousCellDate = $cellDate[0];
        $sousCellNameC = $cellNameC[0];
        $taille = strlen($sousCellNameC[0]);
        $stop = True;
        $i = $taille - 1;

        //On récupère que le lieu de la compétition
        while ($i > 0 && $stop != False) {
            if ($sousCellNameC[0][$i] == ' ') {
                $lieu = substr($sousCellNameC[0], $i + 1);
                $stop = False;
            }
            $i--;
        }

        $nomComp = substr($sousCellNameC[0], 0, $i - 2);
        $date = $sousCellDate[0];

        //On créé un tableau simple de chaque
        foreach ($cellName as $value) {
            $tabNom[] = $value['0'];
        }
        foreach ($cellRep as $value) {
            $tabRep[] = $value['0'];
        }

        // On supprime toutes les lignes qu'on utilise pas
        for ($i = 0; $i < 1000; $i++) {
            if (($tabNom[$i] == null) || (strpos($tabNom[$i], "TROPHEE") === 0) || ($tabNom[$i] == "Liste des inscrits") || ($tabNom[$i] == "Nom et Prénom") || (strpos($tabNom[$i], "Page") === 0) || (strpos($tabNom[$i], "joueurs") === 4)) {
                unset($tabNom[$i]);
            }
            if (($tabRep[$i] == null) || ($tabRep[$i] == "Rep.")) {
                unset($tabRep[$i]);
            }
        }

        //On remet à zéro les indices
        $tabNom = array_values($tabNom);
        $tabRep = array_values($tabRep);

        $tabFinal = array();
        for ($i = 0; $i < sizeof($tabNom); $i++) {
            $tabFinal[$i] = ["nom" => $tabNom[$i], "rep" => $tabRep[$i]];
        }

        $nbJoueurs = count($tabFinal);

        $file = "../public/datas.json";
        $current = json_encode($tabFinal);
        //On écrit en Json dans le fichier
        file_put_contents($file, $current);

        return $this->redirectToRoute('form_validation', array(
            'date' => $date,
            'nomComp' => $nomComp,
            'nbJoueurs' => $nbJoueurs,
            'lieu' => $lieu
        ));

    }
}
