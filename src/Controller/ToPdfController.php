<?php

namespace App\Controller;

use App\Entity\Competition;
use App\Entity\Golf;
use App\Utils\UtilJson;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * for all controller methods in this class
 * @IsGranted("ROLE_USER")
 */
class ToPdfController extends AbstractController
{

    /**
     * @Route("/telechargement/{golf_id}", name="telechargement")
     */
    public function telechargement($golf_id){
        return$this->render('to_pdf/telecharger.html.twig', array(
            'golf_id' => $golf_id
        ));
    }


    /**
     * @Route("/toPdf/{golf_id}", name="to_pdf")
     */
    public function index($golf_id)
    {
        $uj = new UtilJson();

        $tabRep = $uj->TabRep();
        $j = 0;
        $somme = 0;
        $tabJoueurs = [];

        for ($i = 0; $i < count($tabRep); $i++) {
            $somme += $tabRep[$i];
            if ($tabRep[$i] % 3 == 0) {

                while ($j < $somme) {
                    $tabJoueurs[] = ["joueur1" => $uj->parsed_json[$j]->nom,
                        "joueur2" => $uj->parsed_json[$j + 1]->nom,
                        "joueur3" => $uj->parsed_json[$j + 2]->nom];
                    $j += 3;
                }
            } else {
                $temp = $tabRep[$i] % 3;
                if ($temp == 1) {
                    while ($j < $somme) {
                        if ($j == $somme - 4 || $j == $somme - 2) {
                            $tabJoueurs[] = ["joueur1" => $uj->parsed_json[$j]->nom,
                                "joueur2" => $uj->parsed_json[$j + 1]->nom];
                            $j += 2;
                        } else {
                            $tabJoueurs[] = [
                                "joueur1" => $uj->parsed_json[$j]->nom,
                                "joueur2" => $uj->parsed_json[$j + 1]->nom,
                                "joueur3" => $uj->parsed_json[$j + 2]->nom];
                            $j += 3;
                        }

                    }
                } else {
                    while ($j < $somme) {
                        if ($j == $somme - 2) {
                            $tabJoueurs[] = ["joueur1" => $uj->parsed_json[$j]->nom,
                                "joueur2" => $uj->parsed_json[$j + 1]->nom];
                            $j += 2;
                        } else {
                            $tabJoueurs[] = ["joueur1" => $uj->parsed_json[$j]->nom,
                                "joueur2" => $uj->parsed_json[$j + 1]->nom,
                                "joueur3" => $uj->parsed_json[$j + 2]->nom];
                            $j += 3;
                        }
                    }
                }
            }
        }

        $em = $this->getDoctrine()->getManager();
        $golf = $em->getRepository(Golf::class)
            ->find($golf_id);
        $competition = $em->getRepository(Competition::class)
            ->find($golf_id);

        $starter = $competition->getstarter();
        $pos = strpos($starter, ':');

        if ($pos == 1) {
            $minutes = (int)$starter[0] * 60 + (int)substr($starter, -2);
        } else {
            $minutes = (int)substr($starter, 0, 2) * 60 + (int)substr($starter, -2);
        }

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('to_pdf/index.html.twig', array(
            'tabJoueurs' => $tabJoueurs,
            'golfs' => $golf,
            'minutes' => $minutes
        ));

//        dd($competition->getNom());
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();
        $font = $dompdf->getFontMetrics()->get_font("Arial");
        $dompdf->getCanvas()->page_text(765, 560, "Page {PAGE_NUM} / {PAGE_COUNT}", $font, 8, array(0, 0, 0));
        $dompdf->getCanvas()->page_text(20, 25, ucwords(strtolower($competition->getNom())) . " du "
            . ucwords(strtolower($competition->getGolfs()->getLieu())) . " " .
            ucwords($competition->getDate()), $font, 10, array(0, 0, 0));

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => true
        ]);
    }
}
