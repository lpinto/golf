<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * for all controller methods in this class
 *@IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)->findAll();
        return $this->render('admin/index.html.twig',
            array('users' => $users
            ));
    }

    /**
     * @Route("/admin/delete/{id}", name="delete")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $deleteUser = $entityManager->getRepository(User::class)->find($id);


        $entityManager->remove($deleteUser);
        $entityManager->flush();

        return $this->redirectToRoute('admin');
    }
}
