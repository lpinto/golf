<?php

namespace App\Controller;

use App\Entity\Ajout;
use App\Entity\Competition;
use App\Entity\Golf;
use App\Entity\Par;
use App\Entity\Partie;
use App\Entity\Trou;
use App\Form\AjoutType;
use App\Form\PartieType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * for all controller methods in this class
 *@IsGranted("ROLE_USER")
 */

class AjoutController extends AbstractController
{
    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajout(Request $request)
    {
        $nom = new Ajout();
        $form = $this->createForm(AjoutType::class, $nom);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $nom->getNomFichier();
            $fileName = 'file.' . $file->guessExtension();
            $file->move(
                $this->getParameter('upload_directory'),
                $fileName);
            $nom->setNomFichier($fileName);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($nom);
            $entityManager->flush();
            return $this->redirectToRoute('read_excel');

        }

        return $this->render('ajout/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/ajoutPar/{golf_id}", name="ajout_par")
     */

    public function ajout_par($golf_id){
        $par = new Par();

        $em = $this->getDoctrine()->getManager();
        $verifPar = $em->getRepository(Par::class)->findAll();

        if(empty($verifPar)){
            for ($cpt= 1; $cpt < 4; $cpt++){
                $em->clear();

                if ($cpt == 1){
                    $par->setNom('3');
                    $par->setTemps(11);
                }
                elseif ($cpt == 2){
                    $par->setNom('4');
                    $par->setTemps(14);
                }
                else{
                    $par->setNom('5');
                    $par->setTemps(17);
                }
                $em->persist($par);
                $em->flush();
            }
        }

        return $this->redirectToRoute('form_trou', array(
                'golf_id' => $golf_id)
        );
    }

    /**
     * @Route("/form/{lieu}&{date}&{nomComp}&{nbJoueurs}", name="form_validation")
     */

    public function validationForm(Request $request, $lieu, $date, $nomComp, $nbJoueurs)
    {

        $golf = new Golf();
        $golf->setLieu($lieu);


        $competition = new Competition();

        $competition->setNom($nomComp);
        $competition->setDate($date);
        $competition->setGolfs($golf);

        $partie = new Partie();
        $partie->setNbJoueurs($nbJoueurs);
        $partie->setCompetitions($competition);
//        dd($partie);
        $form = $this->createForm(PartieType::class, $partie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $partie = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($partie);
            $em->flush();
//            dd($golf->getId());
            return $this->redirectToRoute('ajout_par', array(
                'golf_id' => $golf->getId()
            ));
        }

        //on rend la vue
        return $this->render('ajout/validation.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/formTrou/{golf_id}", name="form_trou")
     */

    public function TrouForm($golf_id)
    {
        if (empty($_POST)) {
            return $this->render('ajout/informations.html.twig', array(
                'golf_id' => $golf_id
            ));
        }
        $trou = new Trou;
        $em = $this->getDoctrine()->getManager();
        for ($i = 1; $i < 19; $i++) {
            $em->clear();
            $tempsMarche = $_POST["trou" . (string)$i];
            $parTrou = $_POST["parTrou" . (string)$i];
            $trou->setTempsMarche($tempsMarche);
            $trou->setNumero($i);
            $trou->setGolfs($em->getRepository(Golf::class)
                ->find($golf_id));
            $trou->setPars($em->getRepository(Par::class)
                ->find($parTrou));

            $em->persist($trou);
            $em->flush();
        }
        return $this->redirectToRoute('telechargement', array(
            'golf_id' => $golf_id
        ));
    }

}
