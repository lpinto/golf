<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartieRepository")
 */
class Partie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbJoueurs;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbJoueurs(): ?int
    {
        return $this->nbJoueurs;
    }

    public function setNbJoueurs(int $nbJoueurs): self
    {
        $this->nbJoueurs = $nbJoueurs;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Competition", inversedBy="parties", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $competitions;

    public function getCompetitions()
    {
        return $this->competitions;
    }

    public function setCompetitions(Competition $competitions)
    {
        $this->competitions = $competitions;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DecalageTrouPartie", mappedBy="parties")
     */
    private $decalages;

    public function __construct()
    {
        $this->decalages = new ArrayCollection();
    }

    /**
     * Collection|DecalageTrouPartie[]
     */
    public function getDecalages()
    {
        return $this->decalages;
    }
}
