<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DecalageTrouPartieRepository")
 */
class DecalageTrouPartie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $decalage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDecalage(): ?int
    {
        return $this->decalage;
    }

    public function setDecalage(string $decalage): self
    {
        $this->decalage = $decalage;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partie", inversedBy="decalages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parties;

    public function getParties()
    {
        return $this->parties;
    }

    public function setParties($parties): void
    {
        $this->parties = $parties;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trou", inversedBy="decalages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $trous;

    public function getTrous()
    {
        return $this->trous;
    }

    public function setTrous($trous): void
    {
        $this->trous = $trous;
    }
}
