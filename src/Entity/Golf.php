<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GolfRepository")
 */
class Golf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lieu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Trou", mappedBy="golfs")
     */
    private $trous;

    /**
     * @return Collection|Trou[]
     */
    public function getTrous()
    {
        return $this->trous;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Competition", mappedBy="golfs", cascade={"persist"})
     */
    private $competitions;

    public function __construct()
    {
        $this->competitions = new ArrayCollection();
        $this->trous = new ArrayCollection();
    }

    /**
     * @return Collection|Competition[]
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }
}
