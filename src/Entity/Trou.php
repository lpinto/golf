<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrouRepository")
 */
class Trou
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $tempsMarche;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numero;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTempsMarche()
    {
        return $this->tempsMarche;
    }

    /**
     * @param mixed $tempsMarche
     */
    public function setTempsMarche($tempsMarche): void
    {
        $this->tempsMarche = $tempsMarche;
    }



    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero(string $numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Golf", inversedBy="trous")
     * @ORM\JoinColumn(nullable=true)
     */
    private $golfs;

    public function getGolfs()
    {
        return $this->golfs;
    }
    public function setGolfs(Golf $golfs)
    {
        $this->golfs = $golfs;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DecalageTrouPartie", mappedBy="trous")
     */
    private $decalages;

    public function __construct()
    {
        $this->decalages = new ArrayCollection();
    }

    /**
     * Collection|DecalageTrouPartie[]
     */
    public function getDecalages()
    {
        return $this->decalages;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Par", inversedBy="trous")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pars;

    public function getPars()
    {
        return $this->pars;
    }
    public function setPars(Par $pars)
    {
        $this->pars = $pars;
    }
}
