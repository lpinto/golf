<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompetitionRepository")
 */
class Competition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $starter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStarter(): ?string
    {
        return $this->starter;
    }

    public function setStarter(string $starter): self
    {
        $this->starter = $starter;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Golf", inversedBy="competitions", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     *
     * @Assert\Valid()
     * @Assert\Type(type="App\Entity\Golf")
     */
    private $golfs;

    public function getGolfs()
    {
        return $this->golfs;
    }

    public function setGolfs($golfs)
    {
        $this->golfs = $golfs;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Partie", mappedBy="competitions", cascade={"persist"})
     */
    private $parties;

    public function __construct()
    {
        $this->parties = new ArrayCollection();
    }

    /**
     * Collection|Partie[]
     */
    public function getParties()
    {
        return $this->parties;
    }
}
