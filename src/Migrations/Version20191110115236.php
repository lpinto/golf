<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110115236 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ajout (id INT AUTO_INCREMENT NOT NULL, nom_fichier VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competition (id INT AUTO_INCREMENT NOT NULL, golfs_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, date VARCHAR(255) NOT NULL, starter VARCHAR(255) NOT NULL, INDEX IDX_B50A2CB14B3C145B (golfs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE decalage_trou_partie (id INT AUTO_INCREMENT NOT NULL, parties_id INT DEFAULT NULL, trous_id INT DEFAULT NULL, decalage VARCHAR(255) NOT NULL, INDEX IDX_A958D538362AAF23 (parties_id), INDEX IDX_A958D5381ED54061 (trous_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE golf (id INT AUTO_INCREMENT NOT NULL, lieu VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE par (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, temps INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partie (id INT AUTO_INCREMENT NOT NULL, competitions_id INT DEFAULT NULL, nb_joueurs INT NOT NULL, INDEX IDX_59B1F3D14B3F5BE (competitions_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trou (id INT AUTO_INCREMENT NOT NULL, golfs_id INT DEFAULT NULL, pars_id INT DEFAULT NULL, temps_marche INT NOT NULL, numero VARCHAR(255) NOT NULL, INDEX IDX_5066A6324B3C145B (golfs_id), INDEX IDX_5066A632D1347355 (pars_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competition ADD CONSTRAINT FK_B50A2CB14B3C145B FOREIGN KEY (golfs_id) REFERENCES golf (id)');
        $this->addSql('ALTER TABLE decalage_trou_partie ADD CONSTRAINT FK_A958D538362AAF23 FOREIGN KEY (parties_id) REFERENCES partie (id)');
        $this->addSql('ALTER TABLE decalage_trou_partie ADD CONSTRAINT FK_A958D5381ED54061 FOREIGN KEY (trous_id) REFERENCES trou (id)');
        $this->addSql('ALTER TABLE partie ADD CONSTRAINT FK_59B1F3D14B3F5BE FOREIGN KEY (competitions_id) REFERENCES competition (id)');
        $this->addSql('ALTER TABLE trou ADD CONSTRAINT FK_5066A6324B3C145B FOREIGN KEY (golfs_id) REFERENCES golf (id)');
        $this->addSql('ALTER TABLE trou ADD CONSTRAINT FK_5066A632D1347355 FOREIGN KEY (pars_id) REFERENCES par (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE partie DROP FOREIGN KEY FK_59B1F3D14B3F5BE');
        $this->addSql('ALTER TABLE competition DROP FOREIGN KEY FK_B50A2CB14B3C145B');
        $this->addSql('ALTER TABLE trou DROP FOREIGN KEY FK_5066A6324B3C145B');
        $this->addSql('ALTER TABLE trou DROP FOREIGN KEY FK_5066A632D1347355');
        $this->addSql('ALTER TABLE decalage_trou_partie DROP FOREIGN KEY FK_A958D538362AAF23');
        $this->addSql('ALTER TABLE decalage_trou_partie DROP FOREIGN KEY FK_A958D5381ED54061');
        $this->addSql('DROP TABLE ajout');
        $this->addSql('DROP TABLE competition');
        $this->addSql('DROP TABLE decalage_trou_partie');
        $this->addSql('DROP TABLE golf');
        $this->addSql('DROP TABLE par');
        $this->addSql('DROP TABLE partie');
        $this->addSql('DROP TABLE trou');
        $this->addSql('DROP TABLE user');
    }
}
