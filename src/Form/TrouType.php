<?php

namespace App\Form;

use App\Entity\Golf;
use App\Entity\Par;
use App\Entity\Trou;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrouType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tempsMarche', TextType::class)
            ->add('numero', TextType::class)
            ->add('golfs', Golf::class)
            ->add('pars', Par::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trou::class,
        ]);
    }
}
